﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class MetaDisplay : MonoBehaviour {
    public GameObject canvas;

    // colors
    [Header("Colors used to display the preMetaData")]
    [Tooltip("CellCore core color")]
    public Color cellCoreColor;
    [Tooltip("CellCore relation color")]
    public Color cellRelationColor;
    [Tooltip("Node color")]
    public Color nodeColor;
    [Tooltip("Node relation color")]
    public Color nodeRelationColor;
    [Tooltip("the color of the background. Will be ignored if the CellCores are colorized")]
    public Color backGroundColor = Color.white;
    [Tooltip("the color that will be used to display the quad tree")]
    public Color quadTreeColor = Color.black;
    [Tooltip("the color of the first level of the quad tree, only applyed if the related bool is enabled")]
    public Color quadTreeLevelZeroColor = Color.red;
    [Tooltip("uses the color of the biome group over the color of the biome for the coloration")]
    public bool useBiomeGroupColorOverBiomeColor = true;
    // display options
    [Header("Display options")]
    [Tooltip("Drawns the coordinates of the cellCore")]
    public bool drawCellCores = true;
    [Tooltip("Colorizes the cell")]
    public bool drawColorCells = true;
    [Tooltip("draw the relations between CellCores")]
    public bool drawCellRelations = true;
    [Tooltip("Draws the Nodes")]
    public bool drawNodes = true;
    [Tooltip("Draws the relations between the Nodes")]
    public bool drawNodeRelations = true;
    [Tooltip("Display the QuadTree and it's divisions")]
    public bool drawQuadTree = true;
    [Tooltip("alows to set a different color for the roots of the quad tree in order to make them more apparent")]
    public bool quadTreeLevelZeroDifferentColor = true;
    [Tooltip("applies a Perlin noise on the coordinates, only works with the quadtree")]
    public bool deformVoronoi = false;
    // performance options
    [Header("performance options")]
    [Tooltip("use one thread or x threads")]
    public bool useMultiThreadForDisplay = true;
    [Tooltip("number of threads used to calculate the display")]
    [Range(1, 64)]
    public int numberOfThreads = 8;

    private ColorMap colorMap;

    /// <summary>
    /// Method used to create and Instantiate the gameobjects used to display the colormaps
    /// this method is called by the main thread
    /// </summary>
    /// <param name="seed"></param>
    /// <param name="metaData"></param>
    /// <param name="colorMap"></param>
    /// <param name="mapContainer"></param>
    public void applyColorMaps(ref Seed seed, ref PreMetaData preMetaData, ref ColorMap colorMap, ref GameObject mapContainer) {
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                // applying colormap to the plane
                Texture2D texture = new Texture2D(seed.squareRelativeSize / seed.mapDividerScale, seed.squareRelativeSize / seed.mapDividerScale);
                texture.SetPixels(colorMap.colorMap[a][b]);
                texture.Apply();
                GameObject mapElement = Instantiate(canvas, new Vector3(-a * seed.metaDataDisplayScale, 0, -b * seed.metaDataDisplayScale), Quaternion.identity);
                mapElement.name = a.ToString() + ' ' + b.ToString() + " | " + preMetaData.cores[a][b].Length.ToString();
                Renderer textureRender = mapElement.GetComponent<Renderer>();
                textureRender.material.mainTexture = texture;
                textureRender.transform.localScale = new Vector3(seed.mapRelativeSize, 1, seed.mapRelativeSize);
                mapElement.transform.parent = mapContainer.transform;
                mapElement.transform.localScale = new Vector3(seed.metaDataDisplayScale / 10, 1, seed.metaDataDisplayScale / 10);
            }
        }
    }

    /// <summary>
    /// draws all of the map and returns it as a Color[][][]
    /// </summary>
    /// <param name="preMetaData"></param>
    /// <param name="metaData"></param>
    /// <param name="seed"></param>
    /// <returns></returns>
    public ColorMap DrawMap(ref PreMetaData preMetaData, ref MetaData metaData, ref Seed seed) {
        UnityEngine.Debug.Log("started drawing");
        Stopwatch stpw_all = new Stopwatch();
        colorMap = new ColorMap(ref seed, drawColorCells, backGroundColor);
        MetaDisplayAlgorithms mda = new MetaDisplayAlgorithms(cellCoreColor, cellRelationColor, nodeColor, nodeRelationColor, quadTreeColor, quadTreeLevelZeroColor);
        if (drawColorCells) { // coloring cells
            stpw_all.Reset();
            stpw_all.Start();
            if (useMultiThreadForDisplay) {
                mda.multiThreadDraw_CellBiome(seed, metaData, colorMap, deformVoronoi, useBiomeGroupColorOverBiomeColor, numberOfThreads);
            } else {
                mda.draw_CellBiome(ref seed, ref metaData, ref colorMap, deformVoronoi, useBiomeGroupColorOverBiomeColor);
            }
            UnityEngine.Debug.Log("Finished colorizing the cells in " + stpw_all.ElapsedMilliseconds + " ms");
        }
        if (drawCellRelations) {
            stpw_all.Reset();
            stpw_all.Start();
            // WARNING : all lines are drawn twice ( they are seen as half segments )
            mda.draw_CellRelation(ref preMetaData.allCores, ref colorMap, ref seed);
            UnityEngine.Debug.Log("Finished drawing cellrelations in " + stpw_all.ElapsedMilliseconds + " ms");
        }
        if (drawNodeRelations) {
            stpw_all.Reset();
            stpw_all.Start();
            // WARNING : all lines are drawn twice ( they are seen as half segments )
            mda.draw_NodeRelations(ref preMetaData.allNodes, ref colorMap, ref seed);
            UnityEngine.Debug.Log("Finished drawing node ralations in " + stpw_all.ElapsedMilliseconds + " ms");
        }
        if (drawCellCores) {
            stpw_all.Reset();
            stpw_all.Start();
            mda.draw_CellCores(ref preMetaData.allCores, ref colorMap, ref seed);
            UnityEngine.Debug.Log("Finished placing the CellCores in " + stpw_all.ElapsedMilliseconds + " ms");
        }
        if (drawNodes) {
            stpw_all.Reset();
            stpw_all.Start();
            mda.draw_Nodes(ref preMetaData.allNodes, ref colorMap, ref seed);
            UnityEngine.Debug.Log("Finished placing the Nodes in " + stpw_all.ElapsedMilliseconds + " ms");
        }
        if (drawQuadTree) {
            stpw_all.Reset();
            stpw_all.Start();
            mda.draw_QuadTree(ref seed, ref metaData, ref colorMap, quadTreeLevelZeroDifferentColor);
            UnityEngine.Debug.Log("Finished drawing the quad tree in " + stpw_all.ElapsedMilliseconds + " ms");
        }
        return colorMap;
    }
}
