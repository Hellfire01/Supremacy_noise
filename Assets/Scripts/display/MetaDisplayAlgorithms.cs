﻿using UnityEngine;
using System;
using System.Threading;
using System.Collections.Generic;

/// <summary>
/// this class is used by MetaDisplay
/// it contains all of the algorithms
/// </summary>
public class MetaDisplayAlgorithms {

    private Color cellCoreColor;
    private Color cellRelationColor;
    private Color nodeColor;
    private Color nodeRelationColor;
    private Color quadTreeColor;
    private Color quadTreeLevelZeroColor;

    private int quadIndex;
    private int quadCount; // node count is initialized in the multiThreadGeneration method, not in the constructor
    private Mutex mutex;

    /// <summary>
    /// constructor
    /// </summary>
    /// <param name="cellCoreColor"></param>
    /// <param name="cellRelationColor"></param>
    /// <param name="nodeColor"></param>
    /// <param name="nodeRelationColor"></param>
    /// <param name="QuadtreeColor"></param>
    /// <param name="quadTreeLevelZeroColor"></param>
    public MetaDisplayAlgorithms(Color cellCoreColor, Color cellRelationColor, Color nodeColor, Color nodeRelationColor, Color QuadtreeColor, Color quadTreeLevelZeroColor) {
        this.cellCoreColor = cellCoreColor;
        this.cellRelationColor = cellRelationColor;
        this.nodeColor = nodeColor;
        this.nodeRelationColor = nodeRelationColor;
        this.quadTreeColor = QuadtreeColor;
        this.quadTreeLevelZeroColor = quadTreeLevelZeroColor;
    }

    /// <summary>
    /// draws a cross shaped dot on the color map in order to represent nodes or cellcores
    /// </summary>
    /// <param name="colorMap"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="color"></param>
    /// <param name="mapSize"></param>
    /// <param name="squareSize"></param>
    private void draw_dot(ref ColorMap colorMap, int x, int y, Color color, int mapSize, int squareSize) {
        colorMap.setColorOnCoordinates(x / squareSize, y / squareSize, (y % squareSize), (x % squareSize), color);
        if (x - 1 > 0) {
            colorMap.setColorOnCoordinates((x - 1) / squareSize, y / squareSize, (y % squareSize), (x - 1) % squareSize, color);
        }
        if (x + 1 < mapSize) {
            colorMap.setColorOnCoordinates((x + 1) / squareSize, y / squareSize, (y % squareSize), (x + 1) % squareSize, color);
        }
        if (y - 1 > 0) {
            colorMap.setColorOnCoordinates(x / squareSize, (y - 1) / squareSize, ((y - 1) % squareSize), (x % squareSize), color);
        }
        if (y + 1 < mapSize) {
            colorMap.setColorOnCoordinates(x / squareSize, (y + 1) / squareSize, ((y + 1) % squareSize), (x % squareSize), color);
        }
    }

    /// <summary>
    /// draws a line between two neighbor cellcores
    /// </summary>
    /// <param name="cells"></param>
    /// <param name="colorMap"></param>
    /// <param name="seed"></param>
    public void draw_CellRelation(ref List<CellCore> cells, ref ColorMap colorMap, ref Seed seed) {
        foreach (CellCore cell in cells) {
            foreach (CellCore neighbor in cell.neighbors) {
                Lines.drawColorLine(ref colorMap, cellRelationColor, seed.squareRelativeSize,
                    (int)cell.pos.x, (int)cell.pos.y, (int)neighbor.pos.x, (int)neighbor.pos.y);
            }
        }
    }

    /// <summary>
    /// gets the list of the neighbor cellcores in order to correctly colorize
    /// </summary>
    /// <param name="meta"></param>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    private List<CellCore> getListOfCellCores(ref PreMetaData meta, int a, int b, ref Seed seed) {
        List<CellCore> ret = new List<CellCore>();
        for (int x = a - 1; x <= a + 1; x++) {
            for (int y = b - 1; y <= b + 1; y++) {
                if (x >= 0 && y >= 0 && x < seed.mapInitialDivision && y < seed.mapInitialDivision) {
                    ret.AddRange(meta.cores[x][y]);
                }
            }
        }
        return ret;
    }


    /// <summary>
    /// ensures that two threads never calcule the same quadtree
    /// </summary>
    /// <returns></returns>
    private int getQuadIndex() {
        mutex.WaitOne();
        int buffer = -1;
        if (quadIndex < quadCount) {
            buffer = quadIndex;
            quadIndex++;
        }
        mutex.ReleaseMutex();
        return buffer;
    }

    /// <summary>
    /// method that does the coloring of a meta data display chunk
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="seed"></param>
    /// <param name="meta"></param>
    /// <param name="colorMap"></param>
    /// <param name="deformVoronoi"></param>
    /// <param name="useBiomeGroupColorOverBiomeColor"></param>
    private void ColorChunk(int a, int b, Seed seed, MetaData meta, ColorMap colorMap, bool deformVoronoi,
        bool useBiomeGroupColorOverBiomeColor) {
        for (int x = 0; x < seed.squareRelativeSize; x++) {
            for (int y = 0; y < seed.squareRelativeSize; y++) {
                if (x % seed.mapDividerScale == 0 && y % seed.mapDividerScale == 0) {
                    Vector2 buffPos = new Vector2(a * seed.squareRelativeSize + x, b * seed.squareRelativeSize + y);
                    if (deformVoronoi) {
                        // TODO
                    }
                    List<CellCore> buffer = ReadQuadTree.getChildFromCoordinates(meta.quadTree[a, b], buffPos.x, buffPos.y).cellCores;
                    CellCore closest = null;
                    float distance = float.PositiveInfinity;
                    foreach (CellCore cellBuff in buffer) {
                        float buffDistance = Vector2.Distance(buffPos, cellBuff.pos);
                        if (buffDistance < distance) {
                            distance = buffDistance;
                            closest = cellBuff;
                        }
                    }
                    if (useBiomeGroupColorOverBiomeColor) {
                        colorMap.setColorOnCoordinates(a, b, y, x, closest.biomeGroup.color);
                    } else {
                        colorMap.setColorOnCoordinates(a, b, y, x, closest.biome.color);
                    }
                }
            }
        }
    }

    /// <summary>
    /// the multi thread manager for the cell coloration
    /// </summary>
    /// <param name="seed"></param>
    /// <param name="meta"></param>
    /// <param name="colormap"></param>
    /// <param name="deformVoronoi"></param>
    /// <param name="useBiomeGroupColorOverBiomeColor"></param>
    /// <param name="amountOfThreads"></param>
    public void multiThreadDraw_CellBiome(Seed seed, MetaData meta, ColorMap colormap, bool deformVoronoi,
        bool useBiomeGroupColorOverBiomeColor, int amountOfThreads) {
        quadCount = seed.mapInitialDivision * seed.mapInitialDivision;
        quadIndex = 0;
        mutex = new Mutex();
        Thread[] threads = new Thread[amountOfThreads];
        for (int i = 0; i < seed.amountOfWorkerThreads; i++) {
            threads[i] = new Thread(() => { // in thread
                int buff = getQuadIndex();
                while (buff >= 0) {
                    int a = buff / seed.mapInitialDivision;
                    int b = buff % seed.mapInitialDivision;
                    ColorChunk(a, b, seed, meta, colormap, deformVoronoi, useBiomeGroupColorOverBiomeColor);
                    buff = getQuadIndex();
                }
            }); // out of thread
            threads[i].Start();
        }
        for (int i = 0; i < seed.amountOfWorkerThreads; i++) {
            threads[i].Join();
        }
    }

    /// <summary>
    /// draws the content / color of the cell ( taken from the biome )
    /// draws from the quadtree
    /// </summary>
    /// <param name="seed"></param>
    /// <param name="meta"></param>
    /// <param name="colorMap"></param>
    /// <param name="deformVoronoi"></param>
    /// <param name="useBiomeGroupColorOverBiomeColor"></param>
    public void draw_CellBiome(ref Seed seed, ref MetaData meta, ref ColorMap colorMap, bool deformVoronoi, bool useBiomeGroupColorOverBiomeColor) {
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                ColorChunk(a, b, seed, meta, colorMap, deformVoronoi, useBiomeGroupColorOverBiomeColor);
            }
        }
    }

    /// <summary>
    /// draws all of the cellCores
    /// </summary>
    /// <param name="cellCores"></param>
    /// <param name="colorMap"></param>
    /// <param name="seed"></param>
    public void draw_CellCores(ref List<CellCore> cellCores, ref ColorMap colorMap, ref Seed seed) {
        foreach (CellCore buff in cellCores) {
            draw_dot(ref colorMap, (int)buff.pos.x, (int)buff.pos.y, cellCoreColor,
            seed.mapRelativeSize, seed.squareRelativeSize);
        }
    }

    /// <summary>
    /// places the nodes on the map
    /// </summary>
    /// <param name="nodes"></param>
    /// <param name="colorMap"></param>
    /// <param name="seed"></param>
    public void draw_Nodes(ref List<Node> nodes, ref ColorMap colorMap, ref Seed seed) {
        foreach (Node node in nodes) {
            draw_dot(ref colorMap, (int)node.pos.x, (int)node.pos.y, nodeColor,
                seed.mapRelativeSize, seed.squareRelativeSize);
        }
    }

    /// <summary>
    /// draws the line between the different nodes
    /// </summary>
    /// <param name="nodes"></param>
    /// <param name="colorMap"></param>
    /// <param name="seed"></param>
    public void draw_NodeRelations(ref List<Node> nodes, ref ColorMap colorMap, ref Seed seed) {
        foreach (Node node in nodes) {
            foreach (Node sister in node.sisterNodes) {
                if (sister.isOutOfBoundary == false) {
                    Lines.drawColorLine(ref colorMap, nodeRelationColor, seed.squareRelativeSize,
                        (int)node.pos.x, (int)node.pos.y, (int)sister.pos.x, (int)sister.pos.y);
                }
            }
        }
    }

    /// <summary>
    /// se charge de dessiner les contours des feuilles du quad tree
    /// Les valeurs de -1 qui sont présentes dans cette méthode servent à s'assurer que les segments ne soient jamais en dehors de la colormap
    ///     étant donné que les racines des quads trees dessinent les contours des cases, c'est un problème systématique sur les cases en bord de map à droite et en bas
    /// </summary>
    /// <param name="quadTree"></param>
    /// <param name="colorMap"></param>
    private void DrawSquare(ref Seed seed, ref QuadTree quadTree, ref ColorMap colorMap, Color color) {
        // top
        Lines.drawColorLine(ref colorMap, color, seed.squareRelativeSize, (int)quadTree.originPos.x,
            (int)quadTree.originPos.y, (int)(quadTree.originPos.x + quadTree.size) - 1, (int)quadTree.originPos.y);
        // bottom
        Lines.drawColorLine(ref colorMap, color, seed.squareRelativeSize, (int)quadTree.originPos.x,
            (int)(quadTree.originPos.y + quadTree.size) - 1, (int)(quadTree.originPos.x + quadTree.size) - 1, (int)(quadTree.originPos.y + quadTree.size) - 1);
        // left
        Lines.drawColorLine(ref colorMap, color, seed.squareRelativeSize, (int)quadTree.originPos.x,
            (int)quadTree.originPos.y, (int)quadTree.originPos.x, (int)(quadTree.originPos.y + quadTree.size) - 1);
        // right
        Lines.drawColorLine(ref colorMap, color, seed.squareRelativeSize, (int)(quadTree.originPos.x + quadTree.size) - 1,
            (int)quadTree.originPos.y, (int)(quadTree.originPos.x + quadTree.size) - 1, (int)(quadTree.originPos.y + quadTree.size) - 1);
    }

    /// <summary>
    /// se charge d'explorer toutes les divisions du quad tree pour ne déclencher la coloration qu'aux feuilles de l'arbre
    ///     ceci devrait permettre d'optimiser un peu le code
    /// </summary>
    /// <param name="quadTree"></param>
    /// <param name="colorMap"></param>
    private void GoToBottomOfQuadTree(ref Seed seed, QuadTree quadTree, ref ColorMap colorMap) {
        bool end = true;
        if (quadTree.topLeft != null) {
            GoToBottomOfQuadTree(ref seed, quadTree.topLeft, ref colorMap);
            end = false;
        }
        if (quadTree.topRight != null) {
            GoToBottomOfQuadTree(ref seed, quadTree.topRight, ref colorMap);
            end = false;
        }
        if (quadTree.bottomLeft != null) {
            GoToBottomOfQuadTree(ref seed, quadTree.bottomLeft, ref colorMap);
            end = false;
        }
        if (quadTree.bottomRight != null) {
            GoToBottomOfQuadTree(ref seed, quadTree.bottomRight, ref colorMap);
            end = false;
        }
        if (end) {
            DrawSquare(ref seed, ref quadTree, ref colorMap, quadTreeColor);
        }
    }

    /// <summary>
    /// Displays the quad tree
    /// </summary>
    /// <param name="seed"></param>
    /// <param name="metaData"></param>
    /// <param name="colorMap"></param>
    public void draw_QuadTree(ref Seed seed, ref MetaData metaData, ref ColorMap colorMap, bool differentLevelZeroColor) {
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                GoToBottomOfQuadTree(ref seed, metaData.quadTree[a, b], ref colorMap);
            }
        }
        if (differentLevelZeroColor) {
            for (int a = 0; a < seed.mapInitialDivision; a++) {
                for (int b = 0; b < seed.mapInitialDivision; b++) {
                    DrawSquare(ref seed, ref metaData.quadTree[a, b], ref colorMap, quadTreeLevelZeroColor);
                }
            }
        }
    }
}
