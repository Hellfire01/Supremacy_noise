﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorMap {
    public Color[][][] colorMap { get; private set; }
    public int squareSize { get; private set; }
    public int newSquareSize { get; private set; }
    public int divider { get; private set; }

    /// <summary>
    /// constructor
    /// </summary>
    /// <param name="seed"></param>
    public ColorMap(ref Seed seed, bool drawColorCells, Color backGroundColor) {
        colorMap = new Color[seed.mapInitialDivision][][];
        squareSize = seed.squareRelativeSize;
        newSquareSize = seed.squareRelativeSize / seed.mapDividerScale;
        divider = seed.mapDividerScale;
        int size = seed.squareRelativeSize * seed.squareRelativeSize;
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            colorMap[a] = new Color[seed.mapInitialDivision][];
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                Color[] buff = new Color[size];
                if (!drawColorCells) { // should the cells be colorized, there is no point in setting the values as they will be overwroten anyway
                    for (int c = 0; c < size; c++) {
                        buff[c] = backGroundColor;
                    }
                }
                colorMap[a][b] = buff;
            }
        }
    }

    /// <summary>
    /// directly sets the colors
    /// applies a scale diferencial in order to be able to display "smaller" ( less pixels on the end result )
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="y"></param>
    /// <param name="x"></param>
    /// <param name="color"></param>
    public void setColorOnCoordinates(int a, int b, int y, int x, Color color) {
        colorMap[a][b][(y * newSquareSize / squareSize) * newSquareSize + (x * newSquareSize / squareSize)] = color;
    }
}
