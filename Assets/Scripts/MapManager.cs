﻿using System.Diagnostics;
using System.Threading;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MapManager : MonoBehaviour {
    public enum MapMode {
        MetaData,
        TerrainGeneration,
    }

    // meta data or chunks
    [Header("mode")]
    public MapMode mapMode = MapMode.MetaData;
    // metaData
    private Seed seed;
    private MetaDisplay metaDisplay;
    private GameObject map;
    private BiomeManager biomeManager;
    // first level of data
    private PreMetaData FpreMetaData;
    private MetaData FmetaData;
    // second level of data
    private PreMetaData preMetaData;
    private MetaData metaData;
    // benchmarking
    private Stopwatch stpw_all;
    // color map
    private ColorMap colorMap;
    // multi threading
    private bool doneGenerating;
    private bool doneColorizing;
    // GUI
    [Header("GUI")]
    private bool exitedGUI;
    private int messageID;
    private bool wantToExitGUI;
    public Text wittyMessage;
    public Text loadMessage;
    public string messageToDisplay;
    public string[] messages;
    public GameObject GUI_Container;

    //terrain Generation
    TerrainGenerator terrain;

    private Mutex generationLock;
    private Mutex colorationLock;

    /// <summary>
    /// initialises all variables used by the generation
    /// </summary>
    private void InitVariables() {
        GameObject[] buff = GameObject.FindGameObjectsWithTag("map container");
        if (buff.Length != 1) {
            UnityEngine.Debug.LogError("There is not the right amount of game objects with the 'map container' tag. Expected 1, got " + buff.Length);
            return;
        }
        map = buff[0];
        seed = FindObjectOfType<Seed>();
        seed.init();
        biomeManager = new BiomeManager(GetComponent<BiomeDataLinker>(), ref seed);
        metaDisplay = FindObjectOfType<MetaDisplay>();
        stpw_all = new Stopwatch();
        terrain = FindObjectOfType<TerrainGenerator>();
    }

    /// <summary>
    /// initialises the variables and calls the generate method in a separate thread
    /// </summary>
    void Start() {
        messageID = Random.Range(0, messages.Length);
        setLoadingScreen();
        InitVariables();
        colorMap = null;
        doneGenerating = false;
        doneColorizing = false;
        wantToExitGUI = false;
        exitedGUI = false;
        generationLock = new Mutex();
        colorationLock = new Mutex();
        messageToDisplay = "";
        Thread thread = new Thread(() => {
            GenerateFirstLevel();
            GenerateSecondLevel();
        });
        thread.Start();
    }

    /// <summary>
    /// generates the fisrt level of the metaData
    /// </summary>
    public void GenerateFirstLevel() {
        FpreMetaData = new PreMetaData();
        FmetaData = new MetaData();
        // voronoi
        lock (messageToDisplay) {
            messageToDisplay = "generating first voronoi";
        }
        stpw_all.Start();
        MetaGenerator.getMetaData(ref seed, ref FpreMetaData, false);
        UnityEngine.Debug.Log("finished first Voronoï in : " + stpw_all.ElapsedMilliseconds + " ms");
        // set biome groups
        lock (messageToDisplay) {
            messageToDisplay = "attributing biome groups";
        }
        stpw_all.Reset();
        stpw_all.Start();
        BiomeAttribution.setBiomeGroup(ref FpreMetaData, ref biomeManager, ref seed);
        UnityEngine.Debug.Log("finished parent Biome attribution in : " + stpw_all.ElapsedMilliseconds + " ms");
        // cellular automata on biomegroups
        lock (messageToDisplay) {
            messageToDisplay = "applying cellular automata on biome groups";
        }
        stpw_all.Reset();
        stpw_all.Start();
        CellularAutomata.workOnGroups(ref FpreMetaData, ref biomeManager, ref seed);
        UnityEngine.Debug.Log("finished cellular automata on biomegroups in : " + stpw_all.ElapsedMilliseconds + " ms");
        // set quad tree
        lock (messageToDisplay) {
            messageToDisplay = "generating first quad tree";
        }
        stpw_all.Reset();
        stpw_all.Start();
        new QuadTreeGenerator().Parse(seed, FpreMetaData, FmetaData, true);
        UnityEngine.Debug.Log("finished first quadtree in : " + stpw_all.ElapsedMilliseconds + " ms");
    }

    /// <summary>
    /// generates the second level of the metaData
    /// </summary>
    public void GenerateSecondLevel() {
        preMetaData = new PreMetaData();
        metaData = new MetaData();
        // voronoi
        lock (messageToDisplay) {
            messageToDisplay = "generating second voronoi";
        }
        stpw_all.Reset();
        stpw_all.Start();
        MetaGenerator.getMetaData(ref seed, ref preMetaData, true);
        UnityEngine.Debug.Log("finished second Voronoï in : " + stpw_all.ElapsedMilliseconds + " ms");
        // set biomes
        lock (messageToDisplay) {
            messageToDisplay = "attributing biomes";
        }
        stpw_all.Reset();
        stpw_all.Start();
        BiomeAttribution.setBiomesFromParent(ref preMetaData, ref FmetaData, ref biomeManager, ref seed);
        UnityEngine.Debug.Log("finished child Biome attribution in : " + stpw_all.ElapsedMilliseconds + " ms");
        // cellular automata on biomes
        lock (messageToDisplay) {
            messageToDisplay = "applying cellular automata on biomes";
        }
        stpw_all.Reset();
        stpw_all.Start();
        CellularAutomata.workOnBiomes(ref preMetaData, ref biomeManager, ref seed);
        UnityEngine.Debug.Log("finished cellular automata on biomes in : " + stpw_all.ElapsedMilliseconds + " ms");
        // quad tree
        lock (messageToDisplay) {
            messageToDisplay = "generating second level quad tree";
        }
        stpw_all.Reset();
        stpw_all.Start();
        new QuadTreeGenerator().Parse(seed, preMetaData, metaData, false);
        UnityEngine.Debug.Log("finished second quadtree in : " + stpw_all.ElapsedMilliseconds + " ms"); stpw_all.Stop();
        if (mapMode == MapMode.TerrainGeneration) {
            terrain.Init(ref seed, ref metaData);
        }
        if (mapMode == MapMode.MetaData) {
            lock (messageToDisplay) {
                messageToDisplay = "generating map preview";
            }
            // display if the mode is enabled
            colorMap = metaDisplay.DrawMap(ref preMetaData, ref metaData, ref seed);
            colorationLock.WaitOne();
            doneColorizing = true;
            colorationLock.ReleaseMutex();
        }
        generationLock.WaitOne();
        doneGenerating = true;
        generationLock.ReleaseMutex();
        lock (messageToDisplay) {
            messageToDisplay = "press any key to continue";
        }
    }

    /// <summary>
    /// displays the loading GUI
    /// </summary>
    private void setLoadingScreen() {
        if (exitedGUI == false) {
            if (messages.Length != 0) {
                wittyMessage.text = messages[messageID];
            }
        }
    }

    /// <summary>
    /// hides the loading GUI
    /// </summary>
    private void exitLoadScreen() {
        if (exitedGUI == false && wantToExitGUI) {
            if (Input.anyKey) {
                GUI_Container.SetActive(false);
                exitedGUI = true;
            }
        }
    }

    /// <summary>
    /// the update checks if the generation is done or not
    /// </summary>
    private void Update() {
        lock(messageToDisplay) {
            loadMessage.text = messageToDisplay;
        }
        exitLoadScreen();
        switch (mapMode) {
            case (MapMode.MetaData): // case of display of the metaData
                colorationLock.WaitOne();
                if (doneColorizing == true) {
                    metaDisplay.applyColorMaps(ref seed, ref preMetaData, ref colorMap, ref map);
                    doneColorizing = false; // set once again to false in order to avoid the method being called at each update
                    wantToExitGUI = true;
                }
                colorationLock.ReleaseMutex();
                return;
            case (MapMode.TerrainGeneration): // case of the instanciation of the Chunks
                // activer le game object de Jordan ici
                // désactiver le script courant ici
                generationLock.WaitOne();
                if (doneGenerating == true) {
                    terrain.canWork = true;
                    //UnityEngine.Debug.Log("starting drawing");
                    //mapGen.DrawMapInEditor(ref seed, ref metaData);
                    //UnityEngine.Debug.Log("finish drawing");
                    doneGenerating = false;
                    wantToExitGUI = true;
                }
                generationLock.ReleaseMutex();
                return;
            default:
                UnityEngine.Debug.LogError("Unmanaged enum value in map manager mode : " + mapMode);
                return;
        }
    }
}
