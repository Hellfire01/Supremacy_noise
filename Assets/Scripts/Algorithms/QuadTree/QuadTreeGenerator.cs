﻿using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class QuadTreeGenerator {

    private int nodeIndex;
    private int nodeCount; // node count is initialized in the multiThreadGeneration method, not in the constructor
    private Mutex mutex;

    /// <summary>
    /// constructor
    /// </summary>
    public QuadTreeGenerator() {
        nodeIndex = 0;
        mutex = new Mutex();
    }

    /// <summary>
    /// returns a list of all adjacent QuadTrees to the node
    /// this only works because the way the Cellcores are placed makes sur there is no nodes related to nodes in grid cells that are not adjacent
    /// </summary>
    /// <param name="metaData"></param>
    /// <param name="seed"></param>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    private List<QuadTree> getListOfQuadTree(ref MetaData metaData, ref Seed seed, int a, int b) {
        List<QuadTree> ret = new List<QuadTree>();
        for (int x = a - 1; x <= a + 1; x++) {
            for (int y = b - 1; y <= b + 1; y++) {
                if (x >= 0 && y >= 0 && x < seed.mapInitialDivision && y < seed.mapInitialDivision) {
                    ret.Add(metaData.quadTree[x, y]);
                }
            }
        }
        return ret;
    }

    /// <summary>
    /// gets the list of the neighbor cellcores in order to correctly colorize
    /// </summary>
    /// <param name="meta"></param>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    private List<CellCore> getListOfCellCores(ref PreMetaData meta, int a, int b, ref Seed seed) {
        List<CellCore> ret = new List<CellCore>();
        for (int x = a - 1; x <= a + 1; x++) {
            for (int y = b - 1; y <= b + 1; y++) {
                if (x >= 0 && y >= 0 && x < seed.mapInitialDivision && y < seed.mapInitialDivision) {
                    ret.AddRange(meta.cores[x][y]);
                }
            }
        }
        return ret;
    }

    /// <summary>
    /// checks if the node is in the square or not
    /// </summary>
    /// <param name="a"></param>
    /// <param name="quadTree"></param>
    /// <returns></returns>
    private bool isNodeInSquare(Node a, QuadTree quadTree) {
        if (a.pos.x < quadTree.originPos.x || a.pos.y < quadTree.originPos.y || a.pos.x > quadTree.originPos.x + quadTree.size || a.pos.y > quadTree.originPos.y + quadTree.size) {
            return false;
        }
        return true;
    }
    
    /// <summary>
    /// checks if a division is needed or not
    /// all of the operations are sorted from least costly to most costly
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="quadtree"></param>
    /// <returns></returns>
    private bool intersectionCheck(Node a, Node b, ref QuadTree quadtree) {
        float dist = PointToLineDistance.exe(quadtree.center, a.pos, b.pos);
        if (dist <= quadtree.size / 2) {
            return true; // the segment is within the square for sure
        } else if (dist > (quadtree.size / 2) * Mathf.Sqrt(2)) {
            return false; // there is no way the segment is close enough to the square
        }
        // from this point, the node segment is close enough to justify doing an extensive check
        if (SegmentIntersection.exe(quadtree.originPos, quadtree.originPos + new Vector2(quadtree.size, 0), a.pos, b.pos) != Vector2.zero || // top
            SegmentIntersection.exe(quadtree.originPos + new Vector2(quadtree.size, 0), quadtree.originPos + new Vector2(quadtree.size, quadtree.size), a.pos, b.pos) != Vector2.zero || // right
            SegmentIntersection.exe(quadtree.originPos + new Vector2(quadtree.size, quadtree.size), quadtree.originPos + new Vector2(0, quadtree.size), a.pos, b.pos) != Vector2.zero || // bottom
            SegmentIntersection.exe(quadtree.originPos + new Vector2(0, quadtree.size), quadtree.originPos, a.pos, b.pos) != Vector2.zero) { // left
            return true; // there is an intersection with at least one face of the sqare
        }
        if (isNodeInSquare(a, quadtree) || isNodeInSquare(b, quadtree)) {
            return true; // at least one of the 2 nodes is in the square so the quad tree needs an other level
        }
        return false;
    }

    /// <summary>
    /// adds the Cell cores and the related biomes algorithms to the Quadtree while ensuring that there is no duplicates
    /// </summary>
    /// <param name="quadtree"></param>
    /// <param name="list"></param>
    private void addCellCoresToQuadTree(QuadTree quadtree, CellCore[] cellCores) {
        lock (quadtree) {
            foreach (CellCore reference in cellCores) {
                bool isin = false;
                foreach (CellCore buff in quadtree.cellCores) {
                    if (buff.id == reference.id) {
                        isin = true;
                        break;
                    }
                }
                if (isin == false) {
                    quadtree.cellCores.Add(reference);
                    quadtree.biomes.Add(reference.biome);
                }
            }
        }
    }

    /// <summary>
    /// adds the transitions algorithms to the Quadtree while ensuring that there is no duplicates
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    private void addTransitionAlgorithmToQuadTree(QuadTree quadtree, Node a, Node b) {
        lock(quadtree) {
            // TODO
        }
    }

    /// <summary>
    /// adds the cellcores of the nodes to the quadtree and gets the biome algorithms and transition algorithms 
    /// </summary>
    /// <param name="quadtree"></param>
    /// <param name="a"></param>
    /// <param name="b"></param>
    private void setAlgorithmsByNodes(QuadTree quadtree, Node a, Node b) {
        addCellCoresToQuadTree(quadtree, a.parents);
        addCellCoresToQuadTree(quadtree, b.parents);
        addTransitionAlgorithmToQuadTree(quadtree, a, b);
    }

    /// <summary>
    /// adds the cellcore to the quadtree and the related biome algorithm
    /// </summary>
    /// <param name="quadtree"></param>
    /// <param name="preMetaData"></param>
    /// <param name="seed"></param>
    private void setAlgorithmsByClosest(QuadTree quadtree, ref PreMetaData preMetaData, ref Seed seed) {
        List<CellCore> buffer = getListOfCellCores(ref preMetaData, quadtree.a, quadtree.b, ref seed);
        CellCore closest = null;
        float closestDistance = float.PositiveInfinity;
        foreach (CellCore buff in buffer) {
            float distance = Vector2.Distance(quadtree.center, buff.pos);
            if (distance < closestDistance) {
                closestDistance = distance;
                closest = buff;
            }
        }
        addCellCoresToQuadTree(quadtree, new CellCore[] { closest });
        // TODO : add biome algorithm here
        // set transitions to null
    }

    /// <summary>
    /// the recurcive method that divides the quad tree in order to ease the data readings from the chunks
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="parent"></param>
    /// <param name="depth"></param>
    /// <param name="maxDepth"></param>
    private void Divide(Node a, Node b, QuadTree parent, int depth, ref Seed seed, ref PreMetaData preMetaData) {
        bool intersected = intersectionCheck(a, b, ref parent);
        if (parent.hasChildren == false) { // this check insures that the parent does not get biomes attributed to it ( this is due to the way the algorithm works )
            setAlgorithmsByClosest(parent, ref preMetaData, ref seed); // sets the nodes by closest
        }
        if (intersected) {
            if (depth < seed.maxQuadTreeDepth) {
                if (parent.hasChildren == false) {
                    parent.AddNewLayerToQuadTree();
                }
                Divide(a, b, parent.topLeft, depth + 1, ref seed, ref preMetaData);
                Divide(a, b, parent.topRight, depth + 1, ref seed, ref preMetaData);
                Divide(a, b, parent.bottomLeft, depth + 1, ref seed, ref preMetaData);
                Divide(a, b, parent.bottomRight, depth + 1, ref seed, ref preMetaData);
            } else {
                setAlgorithmsByNodes(parent, a, b); // sets the nodes by the intersections with the quadtree
            }
        }
    }

    /// <summary>
    /// ensures that two threads never calculate the same segment
    /// </summary>
    /// <returns></returns>
    private int getNodeIndex() {
        mutex.WaitOne();
        int buffer = -1;
        if (nodeIndex < nodeCount) {
            buffer = nodeIndex;
            nodeIndex++;
        }
        mutex.ReleaseMutex();
        return buffer;
    }

    /// <summary>
    /// method to prevent the quadtyree to divide if the same biome is present on both sides of the segment
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="checkOnBiomeGroup"></param>
    /// <param name="seed"></param>
    /// <returns></returns>
    private bool checkIfSameBiome(Node a, Node b, bool checkOnBiomeGroup, Seed seed) {
        if (seed.divideOnBiomes == false) { // should the user not whant the optimisation, the biome comparaison is disabled here
            return false;
        }
        List<CellCore> relevant = new List<CellCore>();
        foreach(CellCore aCell in a.parents) {
            foreach(CellCore bCell in b.parents) {
                if (aCell.id == bCell.id) {
                    relevant.Add(bCell);
                    break;
                }
            }
        }
        if (relevant.Count != 2) {
            Debug.Log("something is not right / " + relevant.Count);
            return false;
        }
        if (checkOnBiomeGroup) {
            if (relevant[0].biomeGroup.name == relevant[1].biomeGroup.name) {
                return true;
            }
        } else {
            if (relevant[0].biome.name == relevant[1].biome.name) {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// sets the meta data from the premetadata
    /// </summary>
    /// <param name="seed"></param>
    /// <param name="preMetaData"></param>
    /// <param name="metaData"></param>
    public void Parse(Seed seed, PreMetaData preMetaData, MetaData metaData, bool biomeGroup) {
        metaData.quadTree = new QuadTree[seed.mapInitialDivision, seed.mapInitialDivision];
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                Vector2 pos = new Vector2(a * seed.squareRelativeSize, b * seed.squareRelativeSize);
                metaData.quadTree[a, b] = new QuadTree(pos, 0, seed.squareRelativeSize, a, b);
            }
        }
        Thread[] threads = new Thread[seed.amountOfWorkerThreads];
        nodeCount = preMetaData.allNodes.Count;
        for (int i = 0; i < seed.amountOfWorkerThreads; i++) {
            threads[i] = new Thread(() => {
                // in thread
                int index = getNodeIndex();
                while (index != -1) {
                    Node node = preMetaData.allNodes[index];
                    List<QuadTree> quads = getListOfQuadTree(ref metaData, ref seed, node.a, node.b);
                    foreach (Node sister in node.sisterNodes) {
                        if (checkIfSameBiome(node, sister, biomeGroup, seed) == false) { // the division should not be made if the biomes / group biomes are the same
                            foreach (QuadTree quad in quads) {
                                Divide(node, sister, quad, 0, ref seed, ref preMetaData);
                            }
                        }
                    }
                    index = getNodeIndex(); // the loop is not broken until there is no more work to do
                }
                // out of thread
            });
            threads[i].Start();
        }
        for (int i = 0; i < seed.amountOfWorkerThreads; i++) {
            threads[i].Join();
        }
    }
}
