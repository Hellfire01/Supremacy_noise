﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ReadQuadTree {

    /// <summary>
    /// gets the closest of the 4 quadtrees and returns it
    /// </summary>
    /// <param name="quadTree"></param>
    /// <param name="posX"></param>
    /// <param name="posY"></param>
    /// <returns></returns>
    private static QuadTree getClosestQuadTree(QuadTree quadTree, float posX, float posY) {
        QuadTree buffer = quadTree.topLeft;
        Vector2 pos = new Vector2(posX, posY);
        float distance = Vector2.Distance(pos, buffer.center);
        float buffDist = Vector2.Distance(pos, quadTree.topRight.center);
        if (buffDist < distance) {
            buffer = quadTree.topRight;
            distance = Vector2.Distance(pos, buffer.center);
        }
        buffDist = Vector2.Distance(pos, quadTree.bottomRight.center);
        if (buffDist < distance) {
            buffer = quadTree.bottomRight;
            distance = Vector2.Distance(pos, buffer.center);
        }
        buffDist = Vector2.Distance(pos, quadTree.bottomLeft.center);
        if (buffDist < distance) {
            buffer = quadTree.bottomLeft;
        }
        return buffer;
    }

    /// <summary>
    /// returns the closest child quadtree to the given coordinates
    /// </summary>
    /// <param name="quadTree"></param>
    /// <param name="posX"></param>
    /// <param name="posY"></param>
    /// <returns></returns>
    public static QuadTree getChildFromCoordinates(QuadTree quadTree, float posX, float posY) {
        if (quadTree.hasChildren == false) {
            return quadTree;
        } else {
            return getChildFromCoordinates(getClosestQuadTree(quadTree, posX, posY), posX, posY);
        }
    }

    /// <summary>
    /// only gets the parent quadtree and then uses getChildFromCoordinates
    /// </summary>
    /// <param name="map"></param>
    /// <param name="seed"></param>
    /// <param name="posX"></param>
    /// <param name="posY"></param>
    /// <returns></returns>
    public static QuadTree getChildFromCoordinatesAndMap(QuadTree[,] map, ref Seed seed, float posX, float posY) {
        return getChildFromCoordinates(map[(int)posX / seed.squareRelativeSize, (int)posY / seed.squareRelativeSize], posX, posY);
    }

    /// <summary>
    /// makes sure there is no duplicate cellcores in the returned data
    /// </summary>
    /// <param name="quadTree"></param>
    /// <param name="container"></param>
    private static void addQuadtreeDataToRet(QuadTree quadTree, AlgorithmContainer container) {
        foreach(CellCore cell in quadTree.cellCores) {
            bool isin = false;
            foreach(CellCore compare in container.cellcores) {
                if (cell.id == compare.id) {
                    isin = true;
                    break;
                }
            }
            if (isin == false) {
                container.cellcores.Add(cell);
            }
        }
    }

    /// <summary>
    /// gets all of the data for a Chunk
    /// the coordinates are the coordinates of the CENTER of the Chunk
    /// </summary>
    /// <param name="map"></param>
    /// <param name="seed"></param>
    /// <param name="posX"></param>
    /// <param name="posY"></param>
    /// <param name="chunkSize"></param>
    /// <returns></returns>
    public static AlgorithmContainer getAlgoForChunk(QuadTree[,] map, ref Seed seed, float posX, float posY, float chunkSize) {
        AlgorithmContainer ret = new AlgorithmContainer();
        ret.cellcores = new List<CellCore>();
        float halfChunk = chunkSize / 2;
        // the chunk deformation and max transition size are currently not included
        // top left
        addQuadtreeDataToRet(getChildFromCoordinatesAndMap(map, ref seed, Mathf.Clamp(posX - halfChunk - seed.maxTransitionSize, 0f, seed.mapRelativeSize - 1),
            Mathf.Clamp(posY + halfChunk + seed.maxTransitionSize, 0f, seed.mapRelativeSize - 1)), ret);
        // top right
        addQuadtreeDataToRet(getChildFromCoordinatesAndMap(map, ref seed, Mathf.Clamp(posX + halfChunk + seed.maxTransitionSize, 0f, seed.mapRelativeSize - 1),
            Mathf.Clamp(posY + halfChunk + seed.maxTransitionSize, 0f, seed.mapRelativeSize - 1)), ret);
        // botton left
        addQuadtreeDataToRet(getChildFromCoordinatesAndMap(map, ref seed, Mathf.Clamp(posX - halfChunk - seed.maxTransitionSize, 0f, seed.mapRelativeSize - 1),
            Mathf.Clamp(posY - halfChunk - seed.maxTransitionSize, 0f, seed.mapRelativeSize - 1)), ret);
        // bottom right
        addQuadtreeDataToRet(getChildFromCoordinatesAndMap(map, ref seed, Mathf.Clamp(posX + halfChunk + seed.maxTransitionSize, 0f, seed.mapRelativeSize - 1),
            Mathf.Clamp(posY - halfChunk - seed.maxTransitionSize, 0f, seed.mapRelativeSize - 1)), ret);
        return ret;
    }
}
