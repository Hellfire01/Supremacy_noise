﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Lines {
    /// <summary>
    /// private method used to do a swap between two variables
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    private static void swap(ref int a, ref int b) {
        int c = a;
        a = b;
        b = c;
    }
    
    /// <summary>
    /// draws a line between 2 points in a Color[][][] in the given color
    /// </summary>
    /// <param name="map">the color map</param>
    /// <param name="color">the required color</param>
    /// <param name="size">the relative size of one of the squares of the color map</param>
    /// <param name="x0">A.x</param>
    /// <param name="y0">A.y</param>
    /// <param name="x1">B.x</param>
    /// <param name="y1">B.y</param>
    public static void drawColorLine(ref ColorMap colorMap, Color color, int size, int x0, int y0, int x1, int y1) {
        bool steep = false;
        if (Mathf.Abs(x0 - x1) < Mathf.Abs(y0 - y1)) {
            swap(ref x0, ref y0);
            swap(ref x1, ref y1);
            steep = true;
        }
        if (x0 > x1) {
            swap(ref x0, ref x1);
            swap(ref y0, ref y1);
        }
        int dx = x1 - x0;
        int dy = y1 - y0;
        int derror2 = Mathf.Abs(dy) * 2;
        int error2 = 0;
        int y = y0;
        for (int x = x0; x <= x1; x++) {
            if (steep) {
                colorMap.setColorOnCoordinates(y / size, x / size, (x % size), (y % size), color);
            } else {
                colorMap.setColorOnCoordinates(x / size, y / size, (y % size), (x % size), color);
            }
            error2 += derror2;
            if (error2 > dx) {
                y += (y1 > y0 ? 1 : -1);
                error2 -= dx * 2;
            }
        }
    }
}
