﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PointToLineDistance {

    public static float exe(Vector2 point, Vector2 a, Vector2 b) {
        Vector2 n = b - a;
        Vector2 pa = a - point;
        float c = Vector2.Dot(n, pa);
        if (c > 0.0f) { // Closest point is a
            return Mathf.Sqrt(Vector2.Dot(pa, pa));
        }
        Vector2 bp = point - b;
        if (Vector2.Dot(n, bp) > 0.0f) { // Closest point is b
            return Mathf.Sqrt(Vector2.Dot(bp, bp));
        }
        // Closest point is between a and b
        Vector2 e = pa - n * (c / Vector2.Dot(n, n));
        return Mathf.Sqrt(Vector2.Dot(e, e));
    }
}