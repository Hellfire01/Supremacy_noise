﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SegmentIntersection {
    /// <summary>
    /// returns the coordinates of the intersection and returns Vector2.zero if there is none
    /// note : it makes no difference between colinear, parallel and no intersections
    /// </summary>
    /// <param name="p0"></param>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="p3"></param>
    /// <returns></returns>
    public static Vector2 exe(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3) {
        float A1 = p1.y - p0.y;
        float B1 = p0.x - p1.x;
        float C1 = A1 * p0.x + B1 * p0.y;
        float A2 = p3.y - p2.y;
        float B2 = p2.x - p3.x;
        float C2 = A2 * p2.x + B2 * p2.y;
        float denominator = A1 * B2 - A2 * B1;
        if (denominator == 0) {
            if (ArePointsAligned.check(p0, p1, p2)) {
//                Debug.Log("colinear");
            } else {
//                Debug.Log("parallel");
            }
            return Vector2.zero;
        }
        float intersectX = (B2 * C1 - B1 * C2) / denominator;
        float intersectY = (A1 * C2 - A2 * C1) / denominator;
        float rx0 = (intersectX - p0.x) / (p1.x - p0.x);
        float ry0 = (intersectY - p0.y) / (p1.y - p0.y);
        float rx1 = (intersectX - p2.x) / (p3.x - p2.x);
        float ry1 = (intersectY - p2.y) / (p3.y - p2.y);
        if (((rx0 >= 0 && rx0 <= 1) || (ry0 >= 0 && ry0 <= 1)) && ((rx1 >= 0 && rx1 <= 1) || (ry1 >= 0 && ry1 <= 1))) {
            return new Vector2(intersectX, intersectY);
        } else {
//            Debug.Log("no intersection");
            return Vector2.zero;
        }
    }
}
