﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ArePointsAligned {
    public static bool check(Vector2 A, Vector2 B, Vector2 C) {
        return ((B.y - A.y) * (C.x - B.x) == (C.y - B.y) * (B.x - A.x));
    }
}
