﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GetTriangleCircumscribedCircleCenter {
    // donne la valeure absolue de la distance entre deux points
    private static float getDistance(int Xa, int Ya, int Xb, int Yb) {
        return Mathf.Sqrt(Mathf.Pow(Xb - Xa, 2) + Mathf.Pow(Yb - Ya, 2));
    }

    private static void swap(ref int a, ref int b) {
        int c = a;
        a = b;
        b = c;
    }

    // line 1 => passes by A and B
    // line 2 => passes by C and D
    private static Vector2 getIntersection(Vector2 A, Vector2 B, Vector2 C, Vector2 D) { // application de la loi normale des droites qui définit une équation linéaire par 3 valeurs A, B, C
        float A1 = B.y - A.y;
        float B1 = A.x - B.x;
        float C1 = (A1 * A.x) + (B1 * A.y);
        float A2 = D.y - C.y;
        float B2 = C.x - D.x;
        float C2 = (A2 * C.x) + (B2 * C.y);
        float denominator = (A1 * B2) - (A2 * B1);
        return new Vector2((B2 * C1 - B1 * C2) / denominator, (A1 * C2 - A2 * C1) / denominator);
    }

    // method to get the perpandicular vector of the required Vector
    private static Vector2 getPerpendicularVector(ref Vector2 source) {
        return new Vector2(-source.y, source.x);
    }

    // returns the vector of 2 points
    private static Vector2 getVectorOfSegment(ref Vector2 A, ref Vector2 B) {
        return new Vector2(B.x - A.x, B.y - A.y);
    }

    // cette méthode est celle qui retourne les coordonnées du centre circonscrit
    public static Vector2 calc(ref Vector2 A, ref Vector2 B, ref Vector2 C) {
        // obtenir les vecteurs de droite
        Vector2 AB = getVectorOfSegment(ref A, ref B);
        Vector2 AC = getVectorOfSegment(ref A, ref C);
        // obtenir les points des mediatrices
        Vector2 M = A + (AC / 2);
        Vector2 Mp = getPerpendicularVector(ref AC) + M;
        Vector2 O = A + (AB / 2);
        Vector2 Op = getPerpendicularVector(ref AB) + O;
        // obtenir le centre du triangle
        Vector2 center = getIntersection(M, Mp, O, Op);
        return center;
    }

    private static float sqr(float nb) {
        return nb * nb;
    }
    /*
     * tentative de méthode de cacul directe abandonnée car plusgourmande et "cassée" ( elle ne génère pas de bons résultats )
    public static Vector2 calc2(Vector2 A, Vector2 B, Vector2 C) {
        float buffA = A.y * A.y + A.x * A.x;
        float buffB = B.x * B.x + B.y * B.y;
        float y = ((A.x - B.x) * (sqr(C.x) + sqr(C.y) - buffA) - (A.x - C.x) * (buffB - buffA)) /
            (2 * ((A.x - B.x) * (B.y - A.y) - (A.x + C.x) * (C.y - A.y)));
        float x = (buffB - buffA - 2 * (B.y - A.y) * y) /
            (2 * (A.x - B.x));
        return new Vector2(x, y);
    }
    */
}
