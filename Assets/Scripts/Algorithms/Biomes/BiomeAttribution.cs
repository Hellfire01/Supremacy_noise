﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BiomeAttribution {

    /// <summary>
    /// gets the biome group of the parent
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="meta"></param>
    /// <param name="seed"></param>
    private static void setParent(CellCore cell, ref MetaData meta, ref Seed seed) {
        cell.setBiomeGroup(ReadQuadTree.getChildFromCoordinatesAndMap(meta.quadTree, ref seed, cell.pos.x, cell.pos.y).cellCores[0].biomeGroup);
    }

    /// <summary>
    /// gives biomes according to the "parent's" biome
    /// </summary>
    /// <param name="preMeta"></param>
    /// <param name="metaData"></param>
    /// <param name="biomeManager"></param>
    /// <param name="seed"></param>
    public static void setBiomesFromParent(ref PreMetaData preMeta, ref MetaData metaData, ref BiomeManager biomeManager, ref Seed seed) {
        System.Random prng = new System.Random(seed.seedHash);
        foreach (CellCore cell in preMeta.allCores) {
            setParent(cell, ref metaData, ref seed);
            cell.setBiome(cell.biomeGroup.biomes[prng.Next(0, cell.biomeGroup.biomes.Length)]);
        }
    }

    /// <summary>
    /// set les void biomes en bord de map pour la délimiter
    /// </summary>
    /// <param name="meta"></param>
    /// <param name="biomeManager"></param>
    /// <param name="seed"></param>
    private static void setVoidGroup(ref PreMetaData meta, ref BiomeManager biomeManager, ref Seed seed) {
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                if ((a == 0 || a == seed.mapInitialDivision - 1) || (b == 0 || b == seed.mapInitialDivision - 1)) {
                    foreach(CellCore buffer in meta.cores[a][b]) {
                        buffer.setBiomeGroup(biomeManager.voidBiomes[0]);
                    }
                }
            }
        }
    }
    
    /// <summary>
    /// sets the biomes of the cellcores randomly
    /// </summary>
    /// <param name="meta"></param>
    /// <param name="biomeManager"></param>
    /// <param name="seed"></param>
    public static void setBiomeGroup(ref PreMetaData meta, ref BiomeManager biomeManager, ref Seed seed) {
        System.Random prng = new System.Random(seed.seedHash);
        foreach (CellCore cell in meta.allCores) {
            cell.setBiomeGroup(biomeManager.allBiomeGroups[prng.Next(0, biomeManager.allBiomeGroups.Length)]);
        }
        setVoidGroup(ref meta, ref biomeManager, ref seed);
    }
}
