﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CellularRules {
    /// <summary>
    /// makes sur the center of the map is warmer 
    /// </summary>
    /// <param name="preMataData"></param>
    /// <param name="biomeManager"></param>
    /// <param name="seed"></param>
    public static void WarmCenterOfMap(ref PreMetaData preMataData, ref BiomeManager biomeManager, ref Seed seed) {
        System.Random prng = new System.Random(seed.seedHash);
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                if ((a > 1 && a < seed.mapInitialDivision - 2) && (b > 1 && b < seed.mapInitialDivision - 2)) {
                    foreach (CellCore cell in preMataData.cores[a][b]) {
                        foreach (BiomeGroup coldBiome in biomeManager.coldBiomeGroups) {
                            if (cell.biomeGroup.name == coldBiome.name) {
                                cell.setBiomeGroup(biomeManager.warmBiomeGroups[prng.Next(0, biomeManager.warmBiomeGroups.Length - 1)]);
                            }
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// makes sure the border of the map is colder
    /// </summary>
    /// <param name="preMataData"></param>
    /// <param name="biomeManager"></param>
    /// <param name="seed"></param>
    public static void CoolBorderOfMap(ref PreMetaData preMetaData, ref BiomeManager biomeManager, ref Seed seed) {
        System.Random prng = new System.Random(seed.seedHash);
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                if (a == 1 || b == 1 || a == seed.mapInitialDivision - 2 || b == seed.mapInitialDivision - 2) {
                    foreach (CellCore cell in preMetaData.cores[a][b]) {
                        foreach (BiomeGroup hotBiome in biomeManager.hotBiomeGroups) {
                            if (cell.biomeGroup.name == hotBiome.name) {
                                cell.setBiomeGroup(biomeManager.coldBiomeGroups[prng.Next(0, biomeManager.coldBiomeGroups.Length - 1)]);
                            }
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// checks if the cell has a cold neighor
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="biomeManager"></param>
    /// <returns></returns>
    private static bool hasColdNeighbor(CellCore cell, ref BiomeManager biomeManager) {
        foreach (CellCore neighbor in cell.neighbors) {
            foreach (BiomeGroup coldBiome in biomeManager.coldBiomeGroups) {
                if (neighbor.biomeGroup.name == coldBiome.name) {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// ensures that the hot and the cold are not next to each other
    /// </summary>
    /// <param name="preMetaData"></param>
    /// <param name="biomeManager"></param>
    /// <param name="seed"></param>
    public static void correctIncoherences(ref PreMetaData preMetaData, ref BiomeManager biomeManager, ref Seed seed) {
        System.Random prng = new System.Random(seed.seedHash);
        foreach (CellCore cell in preMetaData.allCores) {
            foreach (BiomeGroup hotBiome in biomeManager.hotBiomeGroups) {
                if (cell.biomeGroup.name == hotBiome.name) {
                    if (hasColdNeighbor(cell, ref biomeManager)) {
                        cell.setBiomeGroup(biomeManager.warmBiomeGroups[prng.Next(0, biomeManager.warmBiomeGroups.Length - 1)]);
                    }
                }
            }
        }
    }
}
