﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CellularAutomata {
    /// <summary>
    /// does all of the iterations on the biomes
    /// </summary>
    /// <param name="preMetaData"></param>
    /// <param name="biomeManager"></param>
    /// <param name="seed"></param>
    public static void workOnBiomes(ref PreMetaData preMetaData, ref BiomeManager biomeManager, ref Seed seed) {
        // currently does nothing
    }

    /// <summary>
    /// swaps the biomeGroups of the cells
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    public static void swapCellCoreGroupBiomes(CellCore a, CellCore b) {
        BiomeGroup c = null;
        c = b.biomeGroup;
        b.setBiomeGroup(a.biomeGroup);
        a.setBiomeGroup(b.biomeGroup);
    }

    /// <summary>
    /// gets the average temperature of the neighbors
    /// </summary>
    /// <param name="cell"></param>
    /// <returns></returns>
    public static float getAverageOfNeighbors(CellCore cell) {
        float neighborAvgBuffer = 0;
        foreach (CellCore neighbor in cell.neighbors) {
            neighborAvgBuffer += neighbor.biomeGroup.average;
        }
        return neighborAvgBuffer / cell.neighbors.Length;
    }


    /// <summary>
    /// compares in order to swap
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="difference"></param>
    private static void compareAndSwap(CellCore cell, float difference) {
        float buff = float.PositiveInfinity;
        CellCore bestReplace = null;
        foreach (CellCore neighbor in cell.neighbors) {
            if (neighbor.biomeGroup.isVoid == false) {
                float differencBuffer = Mathf.Abs(cell.biomeGroup.average - getAverageOfNeighbors(neighbor));
                if (differencBuffer < buff) {
                    bestReplace = neighbor;
                    buff = differencBuffer;
                }
            }
        }
        if (buff < difference) {
            swapCellCoreGroupBiomes(cell, bestReplace);
        }
    }

    /// <summary>
    /// compares in order to overwrite
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="difference"></param>
    /// <param name="biomeManager"></param>
    private static void compareAndOverwrite(CellCore cell, float averageTemperature, BiomeManager biomeManager) {
        float diff = float.PositiveInfinity;
        BiomeGroup bestReplace = null;
        foreach (BiomeGroup compare in biomeManager.allBiomeGroups) {
            float buff = Mathf.Abs(averageTemperature - compare.averageTemperature);
            if (buff < diff) {
                diff = buff;
                bestReplace = compare;
            }
        }
        cell.setBiomeGroup(bestReplace);
    }

    /// <summary>
    /// tries to group biome groups in order to have a maximum of consistency
    /// </summary>
    /// <param name="meta"></param>
    /// <param name="biomeManager"></param>
    /// <param name="seed"></param>
    public static void workOnGroups(ref PreMetaData meta, ref BiomeManager biomeManager, ref Seed seed) {
        for (int a = 0; a < seed.nbOfIterations_CA; a++) {
            foreach (CellCore cell in meta.allCores) {
                if (cell.biomeGroup.isVoid == false) {
                    float averageTemperature = getAverageOfNeighbors(cell);
                    float difference = Mathf.Abs(cell.biomeGroup.average - averageTemperature);
                    if (difference > seed.averageOverwriteLimit) {
                        compareAndOverwrite(cell, averageTemperature, biomeManager);
                    } else if (difference > seed.averageSwapLimit) {
                        compareAndSwap(cell, difference);
                    }
                }
            }
        }
        CellularRules.CoolBorderOfMap(ref meta, ref biomeManager, ref seed);
        CellularRules.WarmCenterOfMap(ref meta, ref biomeManager, ref seed);
        CellularRules.correctIncoherences(ref meta, ref biomeManager, ref seed);
    }
}
