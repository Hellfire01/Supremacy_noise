﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fortune {
    public class Point {
        public double x;
        public double y;

        public Point() {
        }

        public void setPoint(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    // use for sites and vertecies
    public class Edge {
        public double a = 0, b = 0, c = 0;
        public CellCore[] ep;
        public CellCore[] reg;
        public int edgenbr;

        public Edge() {
            ep = new CellCore[2];
            reg = new CellCore[2];
        }
    }

    public class Halfedge {
        public Halfedge ELleft, ELright;
        public Edge ELedge;
        public bool deleted;
        public int ELpm;
        public CellCore vertex;
        public double ystar;
        public Halfedge PQnext;

        public Halfedge() {
            PQnext = null;
        }
    }

    public class CoreSorterYX : IComparer<CellCore> {
        public int Compare(CellCore p1, CellCore p2) {
            Vector2 s1 = p1.pos;
            Vector2 s2 = p2.pos;
            if (s1.y < s2.y) return -1;
            if (s1.y > s2.y) return 1;
            if (s1.x < s2.x) return -1;
            if (s1.x > s2.x) return 1;
            return 0;
        }
    }
}
