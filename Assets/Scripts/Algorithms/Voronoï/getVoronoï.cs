﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;
using Fortune;

public static class getVoronoï {
    // this method tests one perticular relation between 2 nodes
    private static bool testRelation(ref CellCore[] parents, ref CellCore[] cousins) {
        int count = 0;
        foreach (CellCore parent in parents) {
            foreach (CellCore cousin in cousins) {
                if (parent.id == cousin.id) {
                    count += 1;
                }
                if (count == 2) {
                    return true;
                }
            }
        }
        return false;
    }

    // method to get all of the sister nodes of a particular Node
    // the sister relation is only done for the targetted node as all of the nodes will be completely evaluated
    private static void getSistersOfNode(Node node, ref PreMetaData meta) {
        int id = node.id; // node
        foreach (CellCore parent in node.parents) { // node->parent
            Node[] related = parent.children;
            foreach (Node toTest in related) { // node->parent->node
                if (toTest.id != id && testRelation(ref node.parents, ref toTest.parents)) {
                    node.addSisterToBuffer(toTest);
                }
            }
        }
    }

    // the nodes are linked after cell generation
    private static void linkMetaData(ref Seed seed, ref PreMetaData meta) {
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                for (int c = 0; c < meta.cores[a][b].Length; c++) {
                    meta.cores[a][b][c].validateBuffers();
                }
            }
        }
        foreach (Node buff in meta.allNodes) {
            buff.validateParentsBuffers();
            getSistersOfNode(buff, ref meta);
            buff.validateBuffers();
        }
        // putting all of the nodes in their arrays
        foreach (Node buff in meta.allNodes) {
            if (buff.pos.x == seed.mapRelativeSize) {
                buff.pos.x = buff.pos.x - 1;
            }
            int x = (int)(buff.pos.x / (int)seed.squareRelativeSize);
            if (buff.pos.y == seed.mapRelativeSize) {
                buff.pos.y = buff.pos.y - 1;
            }
            int y = (int)(buff.pos.y / (int)seed.squareRelativeSize);
            meta.bufferNodes[x][y].Add(buff);
        }
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                meta.nodes[a][b] = meta.bufferNodes[a][b].ToArray();
                for (int c = 0; c < meta.nodes[a][b].Length; c++) {
                    meta.nodes[a][b][c].setValues(a, b, c);
                }
            }
        }
    }

    private static void LaunchFortune(ref Seed seed, ref PreMetaData metaData, ref List<Node> outOfBounds) {
        List<CellCore> sites = new List<CellCore>(metaData.allCores);
        FortuneAlgorithm voroObject = new FortuneAlgorithm(0.1, ref metaData);
        double[] xVal = new double[sites.Count];
        double[] yVal = new double[sites.Count];
        for (int i = 0; i < sites.Count; i++) {
            xVal[i] = sites[i].pos.x;
            yVal[i] = sites[i].pos.y;
        }
        // transforme les sites en list de double et les passe a generate voronoi
        // generate voronoi rempli les meta data comme il faut
        voroObject.generateVoronoi(xVal, yVal, 0, seed.mapRelativeSize, 0, seed.mapRelativeSize);
        metaData.allNodes = voroObject.getNodes();
    }

    public static void getVoronoi(ref Seed seed, ref PreMetaData metaData, ref List<Node> outOfBounds) {
        LaunchFortune(ref seed, ref metaData, ref outOfBounds);
        linkMetaData(ref seed, ref metaData);
    }
}
