﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
This static class's only purpose is to place the cell cores on the map
If required, it will also perform a few checks to ensure the cell cores are "well" placed
 */
public static class CellPlacing {
    // this method ensurs that no CellCores have the same coordinates
    private static Vector2 getPosOfCore(ref CellCore[] cores, int size, int a, int b, ref System.Random prng) {
        Vector2 output = new Vector2(prng.Next(a * size, a * size + size), prng.Next(b * size, b * size + size));
        for (int x = 0; x < cores.Length; x++) {
            if (cores[x] == null) {
                return output;
            }
            if (output == cores[x].pos) {
                return getPosOfCore(ref cores, size, a, b, ref prng);
            }
        }
        return output;
    }

    // this is the private method that is called to set all of the Cores
    public static void SetCores(ref Seed seed, ref PreMetaData metaData, bool secondLevel) {
        System.Random prng = new System.Random(seed.seedHash);
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                int size;
                if (secondLevel) {
                    size = prng.Next(seed.minCellCount * seed.secondLevelCellMultiplier, seed.maxCellCount * seed.secondLevelCellMultiplier);
                }   else {
                    size = prng.Next(seed.minCellCount, seed.maxCellCount);
                }
                metaData.cores[a][b] = new CellCore[size];
                for (int c = 0; c < size; c++) {
                    Vector2 pos = getPosOfCore(ref metaData.cores[a][b], seed.squareRelativeSize, a, b, ref prng);
                    CellCore buff = new CellCore(pos, a, b, c);
                    metaData.cores[a][b][c] = buff;
                    metaData.allCores.Add(buff);
                }
            }
        }
    }
}
