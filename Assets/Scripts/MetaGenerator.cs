﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MetaGenerator {

    /// <summary>
    /// Initializes and returns the PreMetaData class
    /// </summary>
    /// <param name="seed"></param>
    /// <param name="metaData"></param>
    public static void getMetaData(ref Seed seed, ref PreMetaData metaData, bool secondLevel) {
        // initializing all of the variable containers
        List<Node> outOfBounds = new List<Node>();
        metaData.allCores = new List<CellCore>();
        metaData.allNodes = new List<Node>();
        metaData.cores = new CellCore[seed.mapInitialDivision][][];
        metaData.nodes = new Node[seed.mapInitialDivision][][];
        metaData.bufferNodes = new List<Node>[seed.mapInitialDivision][];
        for (int a = 0; a < seed.mapInitialDivision; a++) {
            metaData.cores[a] = new CellCore[seed.mapInitialDivision][];
            metaData.nodes[a] = new Node[seed.mapInitialDivision][];
            metaData.bufferNodes[a] = new List<Node>[seed.mapInitialDivision];
            for (int b = 0; b < seed.mapInitialDivision; b++) {
                metaData.bufferNodes[a][b] = new List<Node>();
            }
        }
        // generating the voronoï and getting the data
        CellPlacing.SetCores(ref seed, ref metaData, secondLevel);
        getVoronoï.getVoronoi(ref seed, ref metaData, ref outOfBounds);
        metaData.outOfBoundaryNodes = outOfBounds.ToArray();
    }
}
