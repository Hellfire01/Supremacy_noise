﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Node {
    public Vector2 pos; // position of the chunk
    [System.NonSerialized]
    public Node[] sisterNodes; // all of the nodes that have a comon parent with this one
    [System.NonSerialized]
    public CellCore[] parents; // the 2 or 3 parents of the node
    // the next 3 variables are used as indexes in the MetaData array
    // ex : MetaData.nodes[a, b, c].id = this.id
    public int a = -1;
    public int b = -1;
    public int c = -1;
    public int id; // valeure unique utilisée pour facilement identifier la classe
    public bool isOutOfBoundary;
    private static int totalNodeCount = 0;

    public Node(Vector2 pos, ref CellCore[] parents, bool isOutOfBoundary) {
        // the variables a, b and c are set after they are placed in the matadata.nodes array
        this.pos = pos;
        this.parents = parents;
        this.sisterNodes = null;
        this.isOutOfBoundary = isOutOfBoundary;
        id = totalNodeCount;
        totalNodeCount += 1;
        sisterBuffer = new List<Node>();
        parentBuffer = new List<CellCore>();
    }

    public void setValues(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    // all of the following variables and methods are used only during the diagramm generation
    // the variables are buffer variables and the methods use those buffers

    [System.NonSerialized]
    private List<Node> sisterBuffer;
    [System.NonSerialized]
    private List<CellCore> parentBuffer;

    public void addSisterToBuffer(Node sister) {
        foreach (Node e in sisterBuffer) {
            if (e.id == sister.id) {
                return;
            }
        }
        sisterBuffer.Add(sister);
    }

    public void addParentToBuffer(CellCore core) {
        parentBuffer.Add(core);
    }

    public void validateParentsBuffers() {
        if (parents.Length == 0) {
            parents = parentBuffer.ToArray();
        }
    }
    public void validateBuffers() {
        sisterNodes = sisterBuffer.ToArray();
    }
}