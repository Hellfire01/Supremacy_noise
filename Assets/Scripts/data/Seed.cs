﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Cette classe doit contenir TOUTES les informations servant à la génération des métadonnées de la carte
 */

public class Seed : MonoBehaviour {

    public enum MapMode {
        Huge,
        Big,
        Medium,
        Small
    }

    [Header("== Map Configuration ==")]
    public MapMode mapMode = MapMode.Small;

    // meta data
    [Header("== Meta data ==")]
    [Tooltip("La seed utilisée pour toutes les metadonnées")]
    public string seed = "xb49C";
    [Tooltip("permet de déterminer la taille des pseudo chunks utilisés pour afficher les métadonnées. Attentions aux valeurs car la caméra est en position fixe")]
    [Range(10, 1000)]
    public float metaDataDisplayScale = 100f;
    [HideInInspector]
    public int mapRelativeSize;
    [HideInInspector]
    public int mapDividerScale;

    // voronoi
    [Header("== Voronoï ==")]
    [HideInInspector]
    public int minCellCount;
    [HideInInspector]
    public int maxCellCount;
    [HideInInspector]
    public int secondLevelCellMultiplier;

    // quadtree
    [Header("== Quadtree ==")]
    [Tooltip("nombre de threads utilisés pour la création du quadtree - nombre recommandé : 8")]
    [Range(1, 64)]
    public int amountOfWorkerThreads = 8;
    [Tooltip("détermine si le quadtree doit être divisé / coupé sur des cellules de même biome / biomegroup")]
    public bool divideOnBiomes = true;
    [HideInInspector]
    public int mapInitialDivision;
    [HideInInspector]
    public int maxQuadTreeDepth;

    // cellular automata
    [Header("== Cellular Automata ==")]
    [Tooltip("number of times the cellular automata is used")]
    [Range(0, 30)]
    public int nbOfIterations_CA = 3;
    [Tooltip("average difference at witch biomes are swapped when the cellular automata iterates")]
    [Range(1f, 10f)]
    public float averageSwapLimit = 3f;
    [Tooltip("average difference at witch biomes are overwritten when the cellular automata iterates - should always be bigger than average swap limit to be used -")]
    [Range(1f, 10f)]
    public float averageOverwriteLimit = 7f;
    [Tooltip("allows the cellular automata to overwrite rather than swap biomes")]
    public bool useBiomeOverwrite = true;

    [Header("== Cellular Automata Rules ==")]
    [Tooltip("replaces all of the ice biomes by warm biomes in the center of the map")]
    public bool warmCenterOfMap = true;

    // implementation
    [Header("== Data Implementation ==")]
    [Range(0f, 1f)]
    public float maxTransitionSizeOnMap = 0.5f;
    [HideInInspector]
    public float maxTransitionSize;
    [HideInInspector]
    public float mapSize;
    [Tooltip("taille d'un Chunk ( nombre de vertexes sur un coté du Chunk ). DOIT être plus petit que la taille minimale d'une feuille du quadtree sur la taille réèlle de la carte")]
    [Range(20, 500)]
    public int chunkSize = 100;
    [Tooltip("taille maximale d'une transition. Utilisée lors de la lecture du quadtree. Divisée par coordinateDivider avant la lecture")]

    [HideInInspector]
    public int seedHash; // used to store the hash of the seed whitch is then used to initialise all of the different speudo random number generators
    [HideInInspector]
    public int squareRelativeSize;

    [Header("== Biome Group Settings ==")]
    public bool continent = true;
    public bool hot = true;
    public bool cold = true;
    public bool water = true;

    private int cellIdCount = 0;
    private int nodeIdCount = 0;

    /// <summary>
    /// sets all of the hiddent parameters for the map
    /// </summary>
    private void setMapConfiguration() {
        switch (mapMode) {
            case (MapMode.Small):
                mapRelativeSize = 2000;
                minCellCount = 5;
                maxCellCount = 8;
                mapInitialDivision = 4;
                maxQuadTreeDepth = 6;
                secondLevelCellMultiplier = 4;
                mapDividerScale = 1;
                break;
            case (MapMode.Medium):
                mapRelativeSize = 4000;
                minCellCount = 4;
                maxCellCount = 8;
                mapInitialDivision = 8;
                maxQuadTreeDepth = 5;
                secondLevelCellMultiplier = 5;
                mapDividerScale = 2;
                break;
            case (MapMode.Big):
                mapRelativeSize = 8000;
                minCellCount = 3;
                maxCellCount = 7;
                mapInitialDivision = 16;
                maxQuadTreeDepth = 5;
                secondLevelCellMultiplier = 6;
                mapDividerScale = 4;
                break;
            case (MapMode.Huge):
                mapRelativeSize = 16000;
                minCellCount = 3;
                maxCellCount = 6;
                mapInitialDivision = 32;
                maxQuadTreeDepth = 5;
                secondLevelCellMultiplier = 7;
                mapDividerScale = 8;
                break;
            default:
                throw new UnityException("Error while setting value of map in seed => unmanaged value " + mapMode);
        }
    }

    /// <summary>
    /// this method initialises the map
    /// </summary>
    public void init() {
        seedHash = Animator.StringToHash(seed);
        squareRelativeSize = (mapRelativeSize / mapInitialDivision);
        setMapConfiguration();
        if (squareRelativeSize * mapInitialDivision != mapRelativeSize) {
            throw new UnityException("pour que la génération procédurale puisse fontionner, la taille relative de la carte DOIT être un multiple de la division initiale de la carte");
        }
    }

    /// <summary>
    /// this method ensures that each Id for each Cell is unique
    /// this also allows to know how many cells there is in the map
    /// </summary>
    /// <returns></returns>
    public int getNewCellId() {
        cellIdCount += 1;
        return cellIdCount - 1;
    }
}
