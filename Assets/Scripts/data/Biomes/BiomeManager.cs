﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeManager {
    public BiomeGroup[] allBiomeGroups { get; private set; }
    public BiomeGroup[] hotBiomeGroups { get; private set; }
    public BiomeGroup[] coldBiomeGroups { get; private set; }
    public BiomeGroup[] warmBiomeGroups { get; private set; }
    public BiomeGroup[] voidBiomes { get; private set; }

    /// <summary>
    /// initialises all of the biomes
    /// </summary>
    /// <param name="linker"></param>
    /// <param name="seed"></param>
    public BiomeManager(BiomeDataLinker linker, ref Seed seed) {
        initBiomes(linker, ref seed);
        initVoidBiomes(linker, ref seed);
    }

    /// <summary>
    /// initilalises the border map biomes
    /// </summary>
    /// <param name="linker"></param>
    /// <param name="seed"></param>
    private void initVoidBiomes(BiomeDataLinker linker, ref Seed seed) {
        List<BiomeGroup> voidBiomesBuffer = new List<BiomeGroup>();
        BiomeGroup _void = new BiomeGroup("Void group", Color.black, true);
        _void.addBiome(new BVoid(ref seed, linker));
        _void.validateBiomeList();
        voidBiomesBuffer.Add(_void);
        voidBiomes = voidBiomesBuffer.ToArray();
    }

    /// <summary>
    /// initilialises the biomes
    /// </summary>
    /// <param name="linker"></param>
    /// <param name="seed"></param>
    private void initBiomes(BiomeDataLinker linker, ref Seed seed) {
        List<BiomeGroup> biomeGroupsBuffer = new List<BiomeGroup>();
        List<BiomeGroup> biomeColdGroupsBuffer = new List<BiomeGroup>();
        List<BiomeGroup> biomeHotGroupsBuffer = new List<BiomeGroup>();
        List<BiomeGroup> biomeWarmGroupsBuffer = new List<BiomeGroup>();
        if (seed.continent == true) {
            BiomeGroup continental = new BiomeGroup("Continental", Color.green, false);
            continental.addBiome(new BGrassLands(ref seed, linker));
            continental.addBiome(new BHills(ref seed, linker));
            continental.addBiome(new BMountains(ref seed, linker));
            continental.validateBiomeList();
            biomeGroupsBuffer.Add(continental);
            biomeWarmGroupsBuffer.Add(continental);
        }
        if (seed.hot == true) {
            BiomeGroup hot = new BiomeGroup("Hot", Color.yellow, false);
            hot.addBiome(new BDesert(ref seed, linker));
            hot.addBiome(new BDesertCanyons(ref seed, linker));
            hot.validateBiomeList();
            biomeGroupsBuffer.Add(hot);
            biomeHotGroupsBuffer.Add(hot);
        }
        if (seed.cold == true) {
            BiomeGroup cold = new BiomeGroup("Cold", Color.white, false);
            cold.addBiome(new BSnowGrassLands(ref seed, linker));
            cold.addBiome(new BSnowHills(ref seed, linker));
            cold.addBiome(new BSnowMountains(ref seed, linker));
            cold.validateBiomeList();
            biomeGroupsBuffer.Add(cold);
            biomeColdGroupsBuffer.Add(cold);
        }
        if (seed.water == true) {
            BiomeGroup water = new BiomeGroup("Water", Color.blue, false);
            water.addBiome(new BSea(ref seed, linker));
            water.addBiome(new BSeaIslands(ref seed, linker));
            water.validateBiomeList();
            biomeGroupsBuffer.Add(water);
            biomeWarmGroupsBuffer.Add(water);
        }
        allBiomeGroups = biomeGroupsBuffer.ToArray();
        hotBiomeGroups = biomeHotGroupsBuffer.ToArray();
        coldBiomeGroups = biomeColdGroupsBuffer.ToArray();
        warmBiomeGroups = biomeWarmGroupsBuffer.ToArray();
    }
}
