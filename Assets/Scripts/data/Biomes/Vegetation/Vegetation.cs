﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Vegetation {
    public string name { get; protected set; }
}
