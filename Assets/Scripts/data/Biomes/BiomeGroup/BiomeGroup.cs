﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeGroup {
    public string name { get; private set; }
    public float averageTemperature { get; private set; }
    public float averageHeight { get; private set; }
    public float averageHumidity { get; private set; }
    public float average { get; private set; }
    public bool isVoid { get; private set; }
    public Biome[] biomes { get; private set; }
    public Color color { get; private set; }
    private List<Biome> buffBiomes;

    /// <summary>
    /// public constructor
    /// the biomes are set separetly
    /// averageX variables are >= -10 && <= 10
    /// </summary>
    /// <param name="_name"></param>
    /// <param name="_averageTemperature"></param>
    /// <param name="_averageHeight"></param>
    /// <param name="_averageHumidity"></param>
    public BiomeGroup(string _name, Color _color, bool _isVoid) {
        this.name = _name;
        this.color = _color;
        this.buffBiomes = new List<Biome>();
        this.isVoid = _isVoid;
    }

    /// <summary>
    /// checks if the biome is in the biomegroup
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public bool isin(Biome target) {
        foreach(Biome compare in biomes) {
            if (compare.name == target.name) {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// makes sure there are no extreme values in the biomes
    /// </summary>
    /// <param name="value"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private float clamp(float value, int min, int max) {
        if (value < min) {
            return min;
        }
        if (value > max) {
            return max;
        }
        return value;
    }
    
    /// <summary>
    /// adds a biome to the buffer
    /// </summary>
    /// <param name="biome"></param>
    public void addBiome(Biome biome) {
        buffBiomes.Add(biome);
    }

    /// <summary>
    /// puts the buffer in the public biome array
    /// </summary>
    public void validateBiomeList() {
        float heightBuffer = 0;
        float tempBuffer = 0;
        float humiBuffer = 0;
        foreach (Biome biome in buffBiomes) {
            heightBuffer += biome.averageHeight;
            humiBuffer += biome.averageHumidity;
            tempBuffer += biome.averageTemperature;
        }
        biomes = buffBiomes.ToArray();
        this.averageHeight = clamp(heightBuffer / biomes.Length, -10, 10);
        this.averageHumidity = clamp(humiBuffer / biomes.Length, -10, 10);
        this.averageTemperature = clamp(tempBuffer / biomes.Length, -10, 10);
        this.average = (this.averageHeight + this.averageHumidity + this.averageTemperature) / 3;
    }
}
