﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Topography {
    public string name { get; protected set; }
    protected int chunkSize;
    public HeightMapSettings HMSettings;
}
