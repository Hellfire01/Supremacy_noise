﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDunes : Topography {
    public TDunes(BiomeDataLinker linker, ref Seed seed) {
        chunkSize = seed.chunkSize;
        name = "Dunes";
        HMSettings = linker.GetHeightMapSettings(name);
    }
}
