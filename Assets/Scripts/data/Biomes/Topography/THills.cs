﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class THills : Topography {
    public THills(BiomeDataLinker linker, ref Seed seed) {
        chunkSize = seed.chunkSize;
        name = "Hills";
        HMSettings = linker.GetHeightMapSettings(name);
    }
}
