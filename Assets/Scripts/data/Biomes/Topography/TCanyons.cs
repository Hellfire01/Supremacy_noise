﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCanyons : Topography {
    public TCanyons(BiomeDataLinker linker, ref Seed seed) {
        chunkSize = seed.chunkSize;
        name = "Canyons";
        HMSettings = linker.GetHeightMapSettings(name);
    }
}
