﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TFlat : Topography {
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="linker"></param>
    /// <param name="seed"></param>
    public TFlat(BiomeDataLinker linker, ref Seed seed) {
        chunkSize = seed.chunkSize;
        name = "Flat";
        HMSettings = linker.GetHeightMapSettings(name);
    }
}
