﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVoid : Topography {

    public TVoid(BiomeDataLinker linker, ref Seed seed) {
        chunkSize = seed.chunkSize;
        name = "Void";
        HMSettings = linker.GetHeightMapSettings(name);
    }

    public float[,] getHeightMap(float posX, float posY) {
        float[,] voidMap = new float[chunkSize,chunkSize];

        for (int y = 0; y < chunkSize; y++) {
            for (int x = 0; x < chunkSize; x++) {
                voidMap[x, y] = 0;
            }
        }
        return voidMap;
    }
}
