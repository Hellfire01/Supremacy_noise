﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TIslands : Topography {
    public TIslands(BiomeDataLinker linker, ref Seed seed) {
        chunkSize = seed.chunkSize;
        name = "Islands";
        HMSettings = linker.GetHeightMapSettings(name);
    }
}
