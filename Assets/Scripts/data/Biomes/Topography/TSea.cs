﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSea : Topography {
    public TSea(BiomeDataLinker linker, ref Seed seed) {
        chunkSize = seed.chunkSize;
        name = "Sea";
        HMSettings = linker.GetHeightMapSettings(name);
    }
}
