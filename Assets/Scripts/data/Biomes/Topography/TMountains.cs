﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TMountains : Topography {
    public TMountains(BiomeDataLinker linker, ref Seed seed) {
        chunkSize = seed.chunkSize;
        name = "Mountains";
        HMSettings = linker.GetHeightMapSettings(name);
    }
}
