﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Transition {
    public string name;
    public Node a;
    public Node b;
    public float Width;
    // X / Y sont les coordonnées en haut à gauche du chunk et X2 / Y2 en bas à gauche. Cela permet de délimiter le chunk avec 4 variables
//    public abstract VertexData getData(float posX, float posY);
}