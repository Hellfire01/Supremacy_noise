﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// important : passer par des prefabs ou l'équivalent pour la sauvegarde des biomes / sous biomes de façon à gérer le plus facilement possible les références sur les autres biomes

[System.Serializable]
public abstract class Biome {
    public string name { get; protected set; }
    public int textureIndex { get; protected set; } // make sure the index of the texture is the good one
    public Topography topography { get; protected set; }
    public Vegetation vegetation { get; protected set; }
    // the averageX values are used by the cellular automata
    public float averageHumidity { get; protected set; } // range : -10 ( super dry ) to 10 ( super wet )
    public float averageHeight { get; protected set; } // range : -10 ( under water ) to 10 ( high mountain )
    public float averageTemperature { get; protected set; } // range : -10 ( freezing ) to 10 ( super hot )
    public Color color; // pour la visualisation des métadonnées. Pas intéresssant dans le cas de la génération des chunks
    protected Seed seed;
    public Material biomeMaterial;
    public TextureData textures;

}
