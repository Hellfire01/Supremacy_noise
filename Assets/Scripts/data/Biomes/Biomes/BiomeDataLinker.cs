﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class is used by the biomes on initialisation whan they require an Animation Curve or any other
///    element that can only be easely configured by the Inspector
/// </summary>
public class BiomeDataLinker : MonoBehaviour {
    public MeshMaterial[] BiomeMaterial;
    public HeightMapSettings[] heightMapSettings;
    public TextureData[] textureDatas;

    public Material GetMeshMaterialFromName(string name) {

        foreach (MeshMaterial meshMat in BiomeMaterial) {
            if (name == meshMat.name) {
                return meshMat.Material;
            }
        }
        throw new UnityException("there was no Material associated to the name " + name);
    }

    public HeightMapSettings GetHeightMapSettings(string name)
    {
        foreach (HeightMapSettings hms in heightMapSettings)
        {
            if (name == hms.name)
            {
                return hms;
            }
        }
        throw new UnityException("there was no Height map setting associated to the name " + name);
    }

    public TextureData GetTextureData(string name)
    {
        foreach (TextureData texture in textureDatas)
        {
            if (name == texture.name)
            {
                return texture;
            }
        }
        throw new UnityException("there was no texture setting associated to the name " + name);
    }
}
