﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSea : Biome {
    public BSea(ref Seed seed, BiomeDataLinker linker) {
        this.name = "Sea";
        this.color = Color.blue;
        this.seed = seed;
        this.averageHeight = 0;
        this.averageHumidity = 2;
        this.averageTemperature = 2;
        this.topography = new TFlat(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
