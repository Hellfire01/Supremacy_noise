﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSnowMountains : Biome {
    public BSnowMountains(ref Seed seed, BiomeDataLinker linker) {
        this.name = "SnowMountains";
        this.color = Color.white;
        this.seed = seed;
        this.topography = new TMountains(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
        this.averageHeight = 8;
        this.averageHumidity = 4;
        this.averageTemperature = -4;
    }
}
