﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSeaIslands : Biome {
    public BSeaIslands(ref Seed seed, BiomeDataLinker linker) {
        this.name = "SeaIslands";
        this.color = Color.blue;
        this.seed = seed;
        this.averageHeight = 0;
        this.averageHumidity = 3;
        this.averageTemperature = 3;
        this.topography = new TFlat(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
