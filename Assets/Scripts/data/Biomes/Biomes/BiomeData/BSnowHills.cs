﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSnowHills : Biome {
    public BSnowHills(ref Seed seed, BiomeDataLinker linker) {
        this.name = "SnowHills";
        this.color = Color.white;
        this.seed = seed;
        this.averageHeight = 3;
        this.averageHumidity = 3;
        this.averageTemperature = -4;
        this.topography = new THills(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
