﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BVoid : Biome {
    public BVoid(ref Seed seed, BiomeDataLinker linker) {
        this.name = "Void";
        this.seed = seed;
        this.color = Color.black;
        this.averageHeight = -10;
        this.averageHumidity = -10;
        this.averageTemperature = -10;
        //test a cause du void
        this.topography = new TVoid(linker, ref seed);
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
