﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSnowGrassLands : Biome {
    public BSnowGrassLands(ref Seed seed, BiomeDataLinker linker) {
        this.name = "SnowGrassLands";
        this.color = Color.white;
        this.seed = seed;
        this.averageHeight = 1;
        this.averageHumidity = 2;
        this.averageTemperature = -2;
        this.topography = new TFlat(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
