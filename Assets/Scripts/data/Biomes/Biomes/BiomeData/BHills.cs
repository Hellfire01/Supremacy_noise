﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BHills : Biome {
    public BHills(ref Seed seed, BiomeDataLinker linker) {
        this.name = "Hills";
        this.color = Color.green;
        this.seed = seed;
        this.averageHeight = 3;
        this.averageHumidity = 2;
        this.averageTemperature = 2;
        this.topography = new THills(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
