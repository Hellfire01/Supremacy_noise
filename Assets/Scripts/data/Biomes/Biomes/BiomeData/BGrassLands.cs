﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGrassLands : Biome {
    /// <summary>
    /// constructor
    /// </summary>
    /// <param name="seed"></param>
    /// <param name="linker"></param>
    public BGrassLands(ref Seed seed, BiomeDataLinker linker) {
        this.name = "GrassLands";
        this.color = Color.green;
        this.seed = seed;
        this.averageHeight = 1;
        this.averageTemperature = 2;
        this.averageHumidity = 2;
        this.topography = new TFlat(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
