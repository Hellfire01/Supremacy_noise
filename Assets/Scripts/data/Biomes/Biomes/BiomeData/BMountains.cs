﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BMountains : Biome {
    public BMountains(ref Seed seed, BiomeDataLinker linker) {
        this.name = "Mountains";
        this.color = Color.gray;
        this.seed = seed;
        this.averageHeight = 7;
        this.averageHumidity = 3;
        this.averageTemperature = 3;
        this.topography = new TMountains(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
