﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BDesert : Biome {
    public BDesert(ref Seed seed, BiomeDataLinker linker) {
        this.name = "Desert";
        this.color = Color.yellow;
        this.seed = seed;
        this.averageHeight = 2;
        this.averageHumidity = -5;
        this.averageTemperature = 8;
        this.topography = new THills(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
