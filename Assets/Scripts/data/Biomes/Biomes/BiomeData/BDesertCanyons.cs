﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BDesertCanyons : Biome {
    public BDesertCanyons(ref Seed seed, BiomeDataLinker linker) {
        this.name = "DesertCanyons";
        this.color = Color.yellow;
        this.seed = seed;
        this.averageHeight = 4;
        this.averageHumidity = -5;
        this.averageTemperature = 8;
        this.topography = new TMountains(linker, ref seed);
        // add vegetation
        this.textureIndex = 0;
        this.biomeMaterial = linker.GetMeshMaterialFromName(this.name);
        this.textures = linker.GetTextureData(name);
    }
}
