﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationCurveAndStringPair {
    public string name;
    public AnimationCurve curve;
}
