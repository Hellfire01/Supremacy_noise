﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MeshMaterial {
    public string name;
    public Material Material;
}
