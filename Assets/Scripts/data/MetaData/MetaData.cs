﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class is used at runtime in order to give the biome and transitions algorithms to the
///     chunks as fast and efficiently as possible
/// </summary>
public class MetaData {
    public QuadTree[,] quadTree;
}
