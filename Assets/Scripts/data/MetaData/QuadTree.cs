﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class QuadTree {
    public int depth { get; private set; } // position relative to the depth of this node in the quadtree
    public int a { get; private set; } // variable used in order to get the position in the meta data array ; variable is shared with children and parents
    public int b { get; private set; } // variable used in order to get the position in the meta data array ; variable is shared with children and parents
    public Vector2 originPos { get; private set; } // absolute position on the map of the top left corner
    public Vector2 center { get; private set; } // absolute position on the map of the center of the node
    public float size { get; private set; } // absolute size of the node on the map
    public QuadTree topLeft { get;  private set; } // top left child node
    public QuadTree topRight { get; private set; } // top rigth child node
    public QuadTree bottomLeft { get; private set; } // bottom left child node
    public QuadTree bottomRight { get; private set; } // bottom rigth child node
    public bool hasChildren { get; private set; } // boolean to know if the node has children nodes without checking all of the 4 variables
    public List<CellCore> cellCores { get; set; } // list of cellcores that are covered by this node
    // algorithms
    public List<Biome> biomes { get; set; } // list of biome algorithms that are covered by this node

    /// <summary>
    /// cretes 4 children for the quadtree
    /// this method is called when a segment of 2 nodes intersects with a quadtree
    /// </summary>
    public void AddNewLayerToQuadTree() {
        lock(this) {
            if (hasChildren == false) { // checking if an other thread hasn't called this method beforehand and created the children
                this.topLeft = new QuadTree(this.originPos, this.depth + 1, this.size / 2, this.a, this.b);
                this.topRight = new QuadTree(this.originPos.x + this.size / 2, this.originPos.y, this.depth + 1, this.size / 2, this.a, this.b);
                this.bottomLeft = new QuadTree(this.originPos.x, this.originPos.y + this.size / 2, this.depth + 1, this.size / 2, this.a, this.b);
                this.bottomRight = new QuadTree(this.originPos.x + this.size / 2, this.originPos.y + this.size / 2, this.depth + 1, this.size / 2, this.a, this.b);
                this.hasChildren = true;
            }
        }
    }

    /// <summary>
    /// method called in order to initialize all of the variables of the constructors
    /// </summary>
    /// <param name="originPos"></param>
    /// <param name="depth"></param>
    /// <param name="size"></param>
    /// <param name="a"></param>
    /// <param name="b"></param>
    private void build(Vector2 originPos, int depth, float size, int a, int b) {
        this.topLeft = null;
        this.topRight = null;
        this.bottomLeft = null;
        this.bottomRight = null;
        this.originPos = originPos;
        this.depth = depth;
        this.size = size;
        this.hasChildren = false;
        this.a = a;
        this.b = b;
        this.center = new Vector2(originPos.x + (size / 2), originPos.y + (size / 2));
        this.cellCores = new List<CellCore>();
        this.biomes = new List<Biome>();
        // init algorithm list
    }

    /// <summary>
    /// constructor from a Vector2 coordinates
    /// </summary>
    /// <param name="originPos"></param>
    /// <param name="depth"></param>
    /// <param name="size"></param>
    /// <param name="a"></param>
    /// <param name="b"></param>
    public QuadTree(Vector2 originPos, int depth, float size, int a, int b) {
        build(originPos, depth, size, a, b);
    }

    /// <summary>
    /// constructor from 2 floats coordinates
    /// </summary>
    /// <param name="posX"></param>
    /// <param name="posY"></param>
    /// <param name="depth"></param>
    /// <param name="size"></param>
    /// <param name="a"></param>
    /// <param name="b"></param>
    public QuadTree(float posX, float posY, int depth, float size, int a, int b) {
        build(new Vector2(posX, posY), depth, size, a, b);
    }
}
